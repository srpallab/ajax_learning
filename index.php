<!doctype html>
<html lang="en">
  <head>
    <meta charset="UTF-8"/>
    <title>AJAX Tuto</title>
    <link href="style.css" rel="stylesheet"/>
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:300,300italic,700,700italic">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/normalize/8.0.1/normalize.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/milligram/1.4.1/milligram.css">
  </head>
  <body class="mw-50 mx-auto">
    <div class="container">
      <div class="row">
	<div class="column">
	  <h1 class="ta-c mx-20">Sign In</h1>
	</div>
      </div>
      <form method="POST">
	<fieldset>
	  <div class="row">
	    <div class="column column-50">
	      <label for="f-name">First Name</label>
	      <input name="f-name" type="text" id="f-name" />
	    </div>
	    <div class="column column-50">
	      <label for="l-name">Last Name</label>
	      <input name="l-name" type="text" id="l-name" />
	    </div>
	  </div>
	  <div class="row">
	    <div class="column">
              <label for="email">Email</label>
              <input name="email" type="email" id="email" />
	    </div>
	  </div>
	  <div class="row">
	    <div class="column">
	      <label for="pwd">Password:</label>
	      <input name="pwd" type="password" id="pwd"/>
	    </div>
	  </div>
	  <div class="row">
	    <div class="column">
              <label for="phn">Phone Number</label>
              <input name="phn" type="text" id="phn"/>
	    </div>
	  </div>
	  <div class="row">
	    <div class="column">
              <input name="submit" type="submit" id="submit" value="Sign In"/>
	    </div>
	  </div>
	</fieldset>
      </form>
    </div>
    <script src="jquery-3.5.1.min.js"></script>
    <script src="main.js"></script>
  </body>
</html>
